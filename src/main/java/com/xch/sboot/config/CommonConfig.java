package com.xch.sboot.config;

import com.xch.sboot.common.thread.ThreadPool;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 集成ThreadPool、Okhttp
 * @author xch
 * 2022/9/6 16:28
 */
@Configuration
public class CommonConfig {
    @Bean
    public ThreadPool getThreadPool() {
        return new ThreadPool(ThreadPool.ThreadEnum.FixedThread, "SBOOT", 8, 16);
    }

    @Bean
    public OkHttpClient getOkHttpClient() {
        return new OkHttpClient();
    }
}
