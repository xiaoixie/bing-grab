package com.xch.sboot.config;

import com.xch.sboot.common.exception.BusinessException;
import com.xch.sboot.common.exception.ExceptionEnum;
import com.xch.sboot.common.response.Result;
import com.xch.sboot.common.util.ErrorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * RestControllerAdvice，统一异常处理
 * @author xch
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerConfig {

    /**
     * 参数校验失败
     * @param e 参数校验异常
     * @return Result
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Result<Void> exceptionHandler(MethodArgumentNotValidException e) {
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.error(ExceptionEnum.BAD_REQUEST.getCode(),
                ExceptionEnum.BAD_REQUEST.getMessage());
    }

    /**
     * 业务异常处理
     *
     * @param e 业务异常
     * @return Result
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public Result<Void> exceptionHandler(BusinessException e) {
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.error(Integer.parseInt(e.getCode()), e.getErrorMsg());
    }

    /**
     * 未知异常处理
     * @return Result
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<Void> exceptionHandler(Exception e) {
        // 把错误信息输入到日志中
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.error(ExceptionEnum.UNKNOWN.getCode(),
                ExceptionEnum.UNKNOWN.getMessage());
    }

    /**
     * 空指针异常
     * @return Result
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public Result<Void> exceptionHandler(NullPointerException e) {
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.error(ExceptionEnum.INTERNAL_SERVER_ERROR.getCode(),
                ExceptionEnum.INTERNAL_SERVER_ERROR.getMessage());
    }

}
