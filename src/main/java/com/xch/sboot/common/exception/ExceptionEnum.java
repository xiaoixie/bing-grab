/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.xch.sboot.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author xch
 * @date 2022/1/10 11:33
 */
public enum ExceptionEnum {

    // 400
    BAD_REQUEST(400, "请求数据格式不正确!"),
    UNAUTHORIZED(401, "登录凭证过期!"),
    FORBIDDEN(403, "没有访问权限!"),
    NOT_FOUND(404, "请求的资源找不到!"),

    // 500
    INTERNAL_SERVER_ERROR(500, "服务器内部错误!"),

    SERVICE_UNAVAILABLE(503, "服务器正忙，请稍后再试!"),

    // 未知异常
    UNKNOWN(10000, "未知异常!"),

    // 自定义
    IS_NOT_NULL(10001,"%s不能为空");



    private int code;
    private String message;

    ExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
