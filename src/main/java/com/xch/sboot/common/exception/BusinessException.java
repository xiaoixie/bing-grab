package com.xch.sboot.common.exception;


/**
 * 自定义业务异常类
 * @author xch
 */
public class BusinessException extends RuntimeException {

    private ExceptionEnum exceptionEnum;
    private String code;
    private String errorMsg;

    public BusinessException() {
        super();
    }

    public BusinessException(ExceptionEnum exceptionEnum) {
        super("{code:" + exceptionEnum.getCode() + ",errorMsg:" + exceptionEnum.getMessage() + "}");
        this.exceptionEnum = exceptionEnum;
        this.code = String.valueOf(exceptionEnum.getCode());
        this.errorMsg = exceptionEnum.getMessage();
    }

    public BusinessException(String errorMsg) {
        super("{code:" + -100 + ",errorMsg:" + errorMsg + "}");
        this.code = "-100";
        this.errorMsg = errorMsg;
    }

    public BusinessException(String code, String errorMsg) {
        super("{code:" + code + ",errorMsg:" + errorMsg + "}");
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public BusinessException(String code, String errorMsg, Object... args) {
        super("{code:" + code + ",errorMsg:" + String.format(errorMsg, args) + "}");
        this.code = code;
        this.errorMsg = String.format(errorMsg, args);
    }

    public ExceptionEnum getExceptionEnum() {
        return exceptionEnum;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "BusinessException{" +
                "exceptionEnum=" + exceptionEnum +
                ", code='" + code + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
