package com.xch.sboot.common.constant;

/**
 * 请求头key
 * @author xch
 * 2022/12/30 13:46
 */
public class RequestHeaders {
    /**
     * 请求头Authorization传输token
     */
    public static final String AUTHORIZATION = "Authorization";
}
