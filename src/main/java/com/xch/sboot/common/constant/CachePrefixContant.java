package com.xch.sboot.common.constant;

/**
 * 缓存前缀常量
 * @author xch
 * 2022/12/30 13:37
 */
public class CachePrefixContant {

    /**
     * 登录token缓存前缀
     */
    public static final String SIGNIN_TOKEN_PREFIX = "SIGNIN:TOKEN:";
}
