package com.xch.sboot.common.util;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Gson 数据解析封装
 * GsonBuilder gsonBuilder = new GsonBuilder();
 * #驼峰转成带点小写（acB——>ac.b）
 * gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DOTS);
 * #驼峰转成带-小写（acB——>ac-b）
 * gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES);
 * #驼峰转成带大写驼峰（acB——>AcB）
 * gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE);
 * #默认驼峰不变（acB——>acB）
 * gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
 * Gson gson = gsonBuilder.create();
 * @author xch
 * 2022/12/29 18:27
 */
public class GsonUtil {

    /**
     * 私有化构造方法
     */
    private GsonUtil() {
    }

    /**
     * 静态内部类做单例
     */
    private static class SingletonHolder {
        private static final Gson GSON = new Gson();
    }

    /**
     * 获取单例方法
     * @return GSON
     */
    public static Gson getInstance() {
        return SingletonHolder.GSON;
    }

    /**
     * 将object对象转成json字符串
     *
     * @param object 对象
     * @return 字符串
     */
    public static String gsonString(Object object) {
        String gsonString = null;
        if (getInstance() != null) {
            gsonString = getInstance().toJson(object);
        }
        return gsonString;
    }

    /**
     * 将gsonString转成泛型bean
     *
     * @param gsonString json字符串
     * @param tClass class
     * @return 对象T
     */
    public static <T> T gsonToBean(String gsonString, Class<T> tClass) {
        T t = null;
        if (getInstance() != null) {
            t = getInstance().fromJson(gsonString, tClass);
        }
        return t;
    }


    /**
     * 转成list
     * 解决泛型问题
     * @param gsonString json字符串
     * @param tClass class
     * @return t类型List
     */
    public static <T> List<T> gsonToList(String gsonString, Class<T> tClass) {
        List<T> list = new ArrayList<T>();
        if (getInstance() != null) {
            JsonArray array = JsonParser.parseString(gsonString).getAsJsonArray();
            for(final JsonElement elem : array){
                list.add(getInstance().fromJson(elem, tClass));
            }
        }
        return list;
    }

    /**
     * json字符串转成JsonElement
     * @param gsonString json字符串
     * @return t类型List
     */
    public static JsonElement gsonToJsonElement(String gsonString) {
        if (getInstance() != null) {
            return JsonParser.parseString(gsonString);
        }
        throw new RuntimeException("GsonUtil gsonToJsonElement RuntimeException");
    }

    /**
     * json字符串转成JsonObject
     * @param gsonString json字符串
     * @return t类型List
     */
    public static JsonObject gsonToJsonObject(String gsonString) {
        if (getInstance() != null) {
            return JsonParser.parseString(gsonString).getAsJsonObject();
        }
        throw new RuntimeException("GsonUtil gsonToJsonObject RuntimeException");
    }


    /**
     * 转成list中有map的
     *
     * @param gsonString json字符串
     * @return List<Map<String, T>>
     */
    @Nullable
    public static <T> List<Map<String, T>> gsonToListMaps(String gsonString) {
        List<Map<String, T>> list = null;
        if (getInstance() != null) {
            list = getInstance().fromJson(gsonString,
                    new TypeToken<List<Map<String, T>>>() {}.getType());
        }
        return list;
    }

    /**
     * json字符串转成map的Map<String, T>
     *
     * @param gsonString json字符串
     * @return Map<String, T>
     */
    @Nullable
    public static <T> Map<String, T> gsonToMaps(String gsonString) {
        Map<String, T> map = null;
        if (getInstance() != null) {
            map = getInstance().fromJson(gsonString, new TypeToken<Map<String, T>>() {
            }.getType());
        }
        return map;
    }

}
