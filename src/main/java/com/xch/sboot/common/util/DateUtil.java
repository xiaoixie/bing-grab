package com.xch.sboot.common.util;

import cn.hutool.core.date.DateField;
import lombok.NonNull;

import java.util.Date;

/**
 * 日期类Util
 * @author xch
 * 2021/7/22 18:28
 */
public class DateUtil {


    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String DATETIME_DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * yyyy-MM-dd
     */
    public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";

    /**
     * yyyy/MM/dd HH:mm:ss
     */
    public static final String DATETIME_SLANT_FORMAT = "yyyy/MM/dd HH:mm:ss";

    /**
     * yyyy/MM/dd
     */
    public static final String DATE_SLANT_FORMAT = "yyyy-MM-dd";

    /**
     * yyyy.MM.dd HH:mm:ss
     */
    public static final String DATETIME_SPOT_FORMAT = "yyyy.MM.dd HH:mm:ss";

    /**
     * yyyy.MM.dd
     */
    public static final String DATE_SPOT_FORMAT = "yyyy.MM.dd";

    /**
     * yyyyMMdd
     */
    public static final String DATE_FORMAT = "yyyyMMdd";


    public static Date offset(@NonNull Date date, @NonNull DateField dateField, int count) {
        return DateUtil.offset(date, dateField, count);
    }

    public static Date beginOfDay(@NonNull Date date) {
        return DateUtil.beginOfDay(date);
    }

    public static Date endOfDay(@NonNull Date date) {
        return DateUtil.endOfDay(date);
    }

    public static String format(@NonNull Date date) {
        return format(date, DATETIME_DEFAULT_FORMAT);
    }

    public static String format(@NonNull Date date, @NonNull String format) {
        return DateUtil.format(date, format);
    }

    public static Date parse(@NonNull String date) {
        return parse(date, DATETIME_DEFAULT_FORMAT);
    }

    public static Date parse(@NonNull String dateStr, @NonNull String format) {
        return DateUtil.parse(dateStr, format);
    }

    public static Date tomorrow(){
        return DateUtil.tomorrow();
    }

    public static Date lastWeek(){
        return DateUtil.lastWeek();
    }

    public static Date nextWeek(){
        return DateUtil.nextWeek();
    }

    public static Date lastMonth(){
        return DateUtil.lastMonth();
    }

    public static Date nextMonth(){
        return DateUtil.nextMonth();
    }

    public static int month(@NonNull Date date) {
        return DateUtil.month(date);
    }

    public static int year(@NonNull Date date) {
        return DateUtil.year(date);
    }

    public static int dayOfMonth(@NonNull Date date) {
        return DateUtil.dayOfMonth(date);
    }

    public static int dayOfWeek(@NonNull Date date) {
        return DateUtil.dayOfWeek(date);
    }

    public static int dayOfYear(@NonNull Date date) {
        return DateUtil.dayOfYear(date);
    }

    public static String getZodiac(@NonNull Date date) {
        int month = month(date);
        int day = dayOfMonth(date);
        return getZodiac(month, day);
    }

    public static String getZodiac(int month, int day) {
        return DateUtil.getZodiac(month, day);
    }

}
