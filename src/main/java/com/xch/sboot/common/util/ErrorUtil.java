package com.xch.sboot.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author xch
 * 2022/7/21 22:13
 */
public class ErrorUtil {

    /**
     * Exception出错的栈信息转成字符串
     * 用于打印到日志中
     */
    public static String errorInfoToString(Throwable e) {
        //try-with-resource语法糖 处理机制
        try(StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw)){
            e.printStackTrace(pw);
            pw.flush();
            sw.flush();
            return sw.toString();
        } catch (Exception exception){
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }
}

