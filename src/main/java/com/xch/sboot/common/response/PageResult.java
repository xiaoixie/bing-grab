package com.xch.sboot.common.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 分页结果封装
 * @author xch
 * 2021/3/7 0:20
 */
@Data
public class PageResult<T> {

    @Data
    @Builder
    public static class PageData<T> {
        private Integer total;
        private Integer page;
        private Integer size;
        private List<T> content;
    }

    private int code;
    private String msg;
    private PageData<T> data;

    public PageResult(List<T> content) {
        this.data = PageData.<T>builder().content(content).build();
    }

    public PageResult(List<T> content, int total, int page, int size) {
        this.data = PageData.<T>builder()
                .content(content)
                .total(total)
                .page(page)
                .size(size)
                .build();
    }

    public PageResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static PageResult ok(String message) {
        return new PageResult(0, message);
    }

    public static PageResult error(String message) {
        return new PageResult(500, message);
    }

    public static PageResult error(int code, String message) {
        return new PageResult(code, message);
    }

    public static <T> PageResult<T> newR(List<T> content) {
        return new PageResult<>(content);
    }

    public static <T> PageResult<T> newR(List<T> content, int total, int page, int size) {
        return new PageResult<>(content, total, page, size);
    }
}