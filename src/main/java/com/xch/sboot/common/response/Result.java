package com.xch.sboot.common.response;

import lombok.Data;

/**
 * 返回数据
 * @author xch
 */
@Data
public class Result<T> {
	private int code;
	private String msg;
	private T data;

	public Result() {
	}

	public Result(T data) {
		this.data = data;
	}

	public Result(T data, String msg) {
		this.data = data;
		this.msg = msg;
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public static Result<Void> ok(String message) {
		return new Result(0, message);
	}

	public static Result<Void> error(String message) {
		return new Result(500, message);
	}

	public static Result<Void> error(int code, String message) {
		return new Result(code, message);
	}

	public static <T> Result<T> newR(T data) {
		return new Result<>(data);
	}

	public static <T> Result<T> newR(T data, String msg) {
		return new Result<>(data, msg);
	}
}
