package com.xch.sboot.service.grab;

import com.xch.sboot.service.grab.impl.BingGrapService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 定时收集
 * @author xch
 * 2023/2/1 14:03
 */
@Component
public class ScheduleCollection {
    @Resource
    private BingGrapService bingGrapService;

    @Scheduled(cron ="0 0 10,11 * * ?")
    //启动执行
    @PostConstruct
    public void collection() {
        bingGrapService.grapImage();
    }

}
